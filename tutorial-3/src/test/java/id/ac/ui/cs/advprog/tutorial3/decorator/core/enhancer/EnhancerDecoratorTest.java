package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        int upgrade;

        upgrade = weapon1.getWeaponValue();
        assertTrue(upgrade >= 50 && upgrade <= 55);

        upgrade = weapon2.getWeaponValue();
        assertTrue(upgrade >= 15 && upgrade <= 20);

        upgrade = weapon3.getWeaponValue();
        assertTrue(upgrade >= 1 && upgrade <= 5);

        upgrade = weapon4.getWeaponValue();
        assertTrue(upgrade >= 5 && upgrade <= 10);

        upgrade = weapon5.getWeaponValue();
        assertTrue(upgrade >= 10 && upgrade <= 15);
    }

}
