package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        guild.addMember(guildMaster,new OrdinaryMember("Sukiman","Juru Perang"));
        assertEquals(2,guild.getMemberList().size());
    }

    @Test
    public void testMethodRemoveMember() {
        guild.addMember(guildMaster,new OrdinaryMember("Sukiman","Juru Perang"));
        guild.removeMember(guildMaster,guild.getMember("Sukiman","Juru Perang"));
        assertEquals(1,guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member child = new OrdinaryMember("Sukiman","Juru Perang");
        guild.addMember(guildMaster,child);
        assertEquals(child,guild.getMember("Sukiman","Juru Perang"));
    }
}
