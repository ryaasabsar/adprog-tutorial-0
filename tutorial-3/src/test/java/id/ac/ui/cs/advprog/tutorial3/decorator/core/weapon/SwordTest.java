package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SwordTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Sword();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Sword",weapon.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals("Great Sword",weapon.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertEquals(25,weapon.getWeaponValue());
    }
}
