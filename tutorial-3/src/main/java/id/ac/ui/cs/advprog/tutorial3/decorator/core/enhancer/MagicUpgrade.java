package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int upgrade;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.upgrade = (int)(Math.random() * 6) + 15;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        if(this.weapon != null) {
            return weapon.getWeaponValue() + upgrade;
        }
        return 0 + upgrade;
    }

    @Override
    public String getDescription() {
        return "Magicly " + weapon.getDescription();
    }
}
