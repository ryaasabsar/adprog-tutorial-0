package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name;
    private String role;
    private List<Member> childList;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.childList = new ArrayList<Member>();
    }

    public String getName(){
        return this.name;
    }

    public String getRole() {
        return this.role;
    }

    public void addChildMember(Member member) {
        if(this.childList.size() < 3 || this.role.equals("Master")) {
            this.childList.add(member);
        }
    }

    public void removeChildMember(Member member) {
        childList.remove(member);
    }

    public List<Member> getChildMembers() {
        return childList;
    }
}
