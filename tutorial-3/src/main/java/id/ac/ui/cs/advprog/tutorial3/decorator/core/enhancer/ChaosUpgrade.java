package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int upgrade;

    public ChaosUpgrade(Weapon weapon) {
        this.weapon= weapon;
        this.upgrade = (int)(Math.random() * 6) + 50;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        if(this.weapon != null) {
            return weapon.getWeaponValue() + upgrade;
        }
        return 0 + upgrade;
    }

    @Override
    public String getDescription() {
        return "Chaosed " + weapon.getDescription();
    }
}
