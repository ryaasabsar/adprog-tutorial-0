package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int upgrade;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.upgrade = (int)(Math.random() * 6) + 5;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        if(this.weapon != null) {
            return weapon.getWeaponValue() + upgrade;
        }
        return 0 + upgrade;
    }

    @Override
    public String getDescription() {
        return "Rawly " + weapon.getDescription();
    }
}
