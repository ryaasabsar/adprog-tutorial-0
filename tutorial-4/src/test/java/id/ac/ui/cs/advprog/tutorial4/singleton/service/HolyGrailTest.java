package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp(){
        holyGrail = new HolyGrail();
    }

    @Test
    public void testGetHolyWish(){
        assertNotNull(holyGrail.getHolyWish());
    }

    @Test
    public void testMakeAWish(){
        holyGrail = spy(holyGrail);
        holyGrail.makeAWish("");

        verify(holyGrail, atLeastOnce()).makeAWish("");
    }

}
