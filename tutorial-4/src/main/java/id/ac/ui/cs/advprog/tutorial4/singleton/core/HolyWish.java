package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {
    private static HolyWish singleton;
    private String wish;

    private HolyWish(){
        wish="wishes";
    }

    public static HolyWish getInstance() {
        if (singleton == null) {
            singleton = new HolyWish();
        }
        return singleton;
    }

    // TODO complete me with any Singleton approach

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
