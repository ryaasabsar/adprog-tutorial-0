package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
        setName("Metal Cluster Knight");
        switch (armory.getClass().getSimpleName()){
            case "DrangleicArmory":
                this.name = "Drangleic " + this.name;
                break;
            case "LordranArmory":
                this.name = "Lordran " + this.name;
                break;
        }

    }

    @Override
    public void prepare() {
        // TODO complete me
        armor = armory.craftArmor();
        skill = armory.learnSkill();
    }

    public String getDescription() {
        return "Armor and Skill";
    }
}
