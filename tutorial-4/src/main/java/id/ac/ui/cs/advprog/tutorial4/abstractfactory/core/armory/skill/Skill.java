package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public interface Skill {

    String getName();
    String getDescription();
}
