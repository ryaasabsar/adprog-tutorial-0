package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
        setName("Synthetic Knight");
        switch (armory.getClass().getSimpleName()){
            case "DrangleicArmory":
                this.name = "Drangleic " + this.name;
                break;
            case "LordranArmory":
                this.name = "Lordran " + this.name;
                break;
        }

    }

    @Override
    public void prepare() {
        // TODO complete me
        skill = armory.learnSkill();
        weapon = armory.craftWeapon();
    }

    public String getDescription() {
        return "Skill and Weapon";
    }
}
