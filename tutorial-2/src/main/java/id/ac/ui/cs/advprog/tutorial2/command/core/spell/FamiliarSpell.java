package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.FamiliarState;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;

    public FamiliarSpell(){
        this.familiar = new Familiar();
    }

    public FamiliarSpell(String race) {
        this.familiar = new Familiar(race);
    }

    @Override
    public void undo() {
        if (familiar.getPrevState() == FamiliarState.ACTIVE) familiar.summon();
        if (familiar.getPrevState() == FamiliarState.SEALED) familiar.seal();
    }
}
