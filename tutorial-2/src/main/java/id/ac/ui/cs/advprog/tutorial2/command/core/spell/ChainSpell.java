package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    protected ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast(){
        for(Spell spell: spells){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        if(!(spells.isEmpty())){
            spells.remove(spells.size()-1);
        }
    }
}
