package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public abstract class HighSpiritSpell implements Spell {
	protected HighSpirit spirit;

	public HighSpiritSpell() {
	    this.spirit = new HighSpirit();
    }

    public HighSpiritSpell(String race) {
        this.spirit = new HighSpirit(race);
    }

    @Override
    public void undo() {
        if (spirit.getPrevState() == HighSpiritState.ATTACK) spirit.attackStance();
        if (spirit.getPrevState() == HighSpiritState.DEFEND) spirit.defenseStance();
        if (spirit.getPrevState() == HighSpiritState.STEALTH) spirit.stealthStance();
        if (spirit.getPrevState() == HighSpiritState.SEALED) spirit.seal();
    }
}
