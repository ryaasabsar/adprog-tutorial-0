package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;

    public ContractSeal() {
        this.spells = new HashMap<>();
        this.latestSpell = null;

    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        latestSpell = spells.get(spellName);
        if(latestSpell != null) latestSpell.cast();
    }

    public void undoSpell() {
        if(latestSpell != null) {
            this.latestSpell.undo();
            latestSpell = null;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
