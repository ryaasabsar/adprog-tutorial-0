package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;
        private List<Adventurer> adventurers;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();

                this.adventurers = new ArrayList<Adventurer>();

                this.agileAdventurer = new AgileAdventurer(this.guild);
                this.addAdventurer(this.agileAdventurer);
                this.knightAdventurer = new KnightAdventurer(this.guild);
                this.addAdventurer(this.knightAdventurer);
                this.mysticAdventurer = new MysticAdventurer(this.guild);
                this.addAdventurer(this.mysticAdventurer);
        }

        //ToDo: Complete Me

        public void addAdventurer(Adventurer adventurer) {
                adventurers.add(adventurer);
        }

        public void addQuest(Quest quest) {
                this.guild.addQuest(quest);
                this.questRepository.save(quest);
        }

        public List<Adventurer> getAdventurers() {
                return this.adventurers;
        }
}
