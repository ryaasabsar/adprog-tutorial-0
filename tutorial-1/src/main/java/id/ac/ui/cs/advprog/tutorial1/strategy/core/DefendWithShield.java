package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "SHIELD";
    }

    @Override
    public String getType(){
        return "MYSTIC DEFEND";
    }
}
