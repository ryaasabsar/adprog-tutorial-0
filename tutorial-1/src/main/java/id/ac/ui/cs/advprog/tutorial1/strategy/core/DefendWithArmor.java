package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "ARMOR";
    }

    @Override
    public String getType(){
        return "KNIGHT DEFEND";
    }
}
