package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                this.guild = guild;
                guild.add(this);
                //ToDo: Complete Me
        }

        //ToDo: Complete Me
        @Override
        public void update(){
                Quest quest = this.guild.getQuest();
                String questType = this.guild.getQuestType();
                this.addQuest(quest);
                /*if(questType.equals("D") || questType.equals("E") || questType.equals("R")){
                        this.addQuest(quest);
                }*/
        }
}
