package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                guild.add(this);

        }

        //ToDo: Complete Me
        @Override
        public void update(){
                Quest quest = this.guild.getQuest();
                String questType = this.guild.getQuestType();
                if(questType.equals("D") || questType.equals("R")){
                        this.addQuest(quest);
                }
        }
}
