package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                guild.add(this);
                //ToDo: Complete Me
        }

        //ToDo: Complete Me
        @Override
        public void update(){
                Quest quest = this.guild.getQuest();
                String questType = this.guild.getQuestType();
                if(questType.equals("D") || questType.equals("E")){
                        this.addQuest(quest);
                }
        }
}
