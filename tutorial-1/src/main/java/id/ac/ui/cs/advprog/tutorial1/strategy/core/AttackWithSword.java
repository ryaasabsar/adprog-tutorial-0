package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me
    public String attack(){
        return "SWORD";
    }

    @Override
    public String getType(){
        return "KNIGHT ATTACK";
    }
}
