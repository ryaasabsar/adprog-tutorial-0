package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me
    public String attack(){
        return "GUN";
    }

    @Override
    public String getType(){
        return "AGILE ATTACK";
    }
}
