package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "Barrier";
    }

    @Override
    public String getType(){
        return "AGILE DEFEND";
    }
}
