package id.ac.ui.cs.advprog.tutorial5.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {
    Soul soul;

    @BeforeEach
    public void setUp() {
        soul = new Soul("Glow",2415,"Unknown","Brightest Light");
        soul.setId(1);
    }

    @Test
    public void testSetId() {
        soul.setId(2);
        assertEquals(2, soul.getId());
    }

    @Test
    public void testSetName() {
        soul.setName("GlowFanatic");
        assertEquals("GlowFanatic", soul.getName());
    }

    @Test
    public void testSetAge() {
        soul.setAge(5);
        assertEquals(5, soul.getAge());
    }

    @Test
    public void testSetGender() {
        soul.setGender("Male");
        assertEquals("Male", soul.getGender());
    }

    @Test
    public void testSetOccupation() {
        soul.setOccupation("Fanatic");
        assertEquals("Fanatic", soul.getOccupation());
    }

    @Test
    public void testGetName() {
        assertEquals("Glow", soul.getName());
    }

    @Test
    public void testGetAge() {
        assertEquals(2415, soul.getAge());
    }

    @Test
    public void testGetGender() {
        assertEquals("Unknown", soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        assertEquals("Brightest Light", soul.getOccupation());
    }


}
